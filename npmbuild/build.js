var exec = require('child_process').exec,
    path = require('path'),
    cmd, cwd, opts;

cwd = path.resolve(__dirname + '/example');

cmd = '';
// cmd += 'cd ' + cwd + '; if [ ! -d node_modules ]; then mkdir node_modules; fi; ';
cmd += 'npm install . --loglevel silent'; // pack@ver.si.on

console.log(cwd);
console.log(cmd);

// npm install . --loglevel silent

exec(cmd, opts, function(err, stdout, stderr) {
  console.log('err', err);
  console.log('stdout', stdout);
  console.log('stderr', stderr);
});
